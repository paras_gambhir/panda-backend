var express = require('express');
var router = express.Router();
var businessUser = require('../src/business/user');
var customerUser = require('../src/customer/user');
var transaction = require('../src/customer/transaction');
var util = require('../src/commonFunctions');
var offer = require('../src/business/offer');



/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.post('/business/signup',businessUser.signup);
router.post('/business/login',businessUser.login);
router.post('/business/profile', util.getBusinessIDfromAcessToken, businessUser.profile);
router.post('/business/createOffer', util.getBusinessIDfromAcessToken, offer.storeOfferController);
router.post('/business/editOffer', util.getBusinessIDfromAcessToken, offer.editOfferController);
router.post('/customer/signup',customerUser.signup);
router.post('/customer/login',customerUser.login);
router.post('/customer/getOffers',util.getCustomerIDfromAcessToken, offer.getNearbyOffersController);
router.post('/business/getOffers',util.getBusinessIDfromAcessToken, offer.getOffersforBusiness);
router.post('/customer/redeemCoupon', util.getCustomerIDfromAcessToken, transaction.redeemCoupon);

module.exports = router;
