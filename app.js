var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');

var routes = require('./routes/index');
var users = require('./routes/users');
var debug = require('debug')('panda-backend:server');
var http = require('http');
require('./mysqlLib');
var moment_ist = require('moment-timezone').tz.setDefault('Asia/Kolkata');

var app = express();

// Set up CORS
app.use(cors());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


// development error handler
// will print stacktrace
if (app.get('env') !== 'production') {
    app.use(function (err, req, res, next) {
        var status = err.status || 500;
        res.status(status);
        var now = moment_ist().format('llll');
        err.stack = 'Time: ' + now + '\n' + err.stack;
        err.stack = 'Status: ' + status + '\n' + err.stack;
        if (err.context) {
            err.stack = 'Message: ' + err.context + '\n' + err.stack;
        }
        if (err.query) {
            err.stack = 'Query: ' + err.query + '\n' + err.stack;
        }
        process.stderr.write(err.stack);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
app.use(function (err, req, res, next) {
    var status = err.status || 500;
    res.status(status);
    var now = moment_ist().format('llll');
    err.stack = 'Time: ' + now + '\n' + err.stack;
    err.stack = 'Status: ' + status + '\n' + err.stack;
    if (err.context) {
        err.stack = 'Context: ' + err.context + '\n' + err.stack;
    }
    if (err.query) {
        err.stack = 'Query: ' + err.query + '\n' + err.stack;
    }
    process.stderr.write(err.stack);
    res.send({
        message: err.context,
        status: err.status
    });
});

/**
 * Get port from environment and store in Express.
 */
var port = normalizePort(process.env.PORT || '8030');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port, function (err) {
    if (err) {
        console.log("Couldn't connect to node.");
    }
    console.log("App listening at :", port);
});
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string' ?
        'Pipe ' + port :
        'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string' ?
        'pipe ' + addr :
        'port ' + addr.port;
    debug('Listening on ' + bind);
}



module.exports = app;