/**
 * Created by harmandeepkaur on 29/10/16.
 */

var util = require('../commonFunctions');
var thisis = this;


/**
 * business user signup page call (POST CALL) (PATH: /business/signup)
 * @param {string} req.body.email email of the business
 * @param {string} req.body.password Encrypted password of the business
 * @param {string} req.body.address address of the business
 * @param {string} req.body.nameOfBusiness name Of Business
 * @param {float} req.body.lat latitude of the business
 * @param {float} req.body.long longitude of the business
 * @param {string} req.body.phone phone of the business
 * @param {string} req.body.primaryContactName primaryContactName of the business
 * @param {object} res node Response object
 */
exports.signup = function (req, res, next) {
    var userObj = {};

    if (!req.body.email || !req.body.password || !req.body.nameOfBusiness || !req.body.address
        || !req.body.lat || !req.body.long || !req.body.phone || !req.body.primaryContactName) {

        return next(util.createError(null, 422, 'Missing parameters. Unable to process.'));
    }

    userObj['email'] = req.body.email;
    userObj['password'] = req.body.password;
    userObj['nameOfBusiness'] = req.body.nameOfBusiness;
    userObj['address'] = req.body.address;
    userObj['lat'] = req.body.lat;
    userObj['long'] = req.body.long;
    userObj['phone'] = req.body.phone;
    userObj['primaryContactName'] = req.body.primaryContactName;

    thisis.signupBusiness(userObj, function (err,accessToken) {
        if (err) {
            return next(err);
        }
        res.send({  'Status':200,
                    'Message':'saved business details',
                    'data':{
                                'accessToken': accessToken
                            }
        });
    });
};

/**
 *
 * @param {object} userDetails
 * @param {function} callback
 */
exports.signupBusiness = function (userDetails, callback) {

    util.AccessToken(function (errAccessToken, accessToken) {
        if(errAccessToken){
            return callback(errAccessToken);
        }
        var sql = 'INSERT INTO `business_user` (`email`, `password`, `name`, `address`, `phone`, ' +
            '`lat`, `long`, `is_active`, `access_token`, `primary_contact_name`) ' +
            'VALUES ' +
            '(?,?, ?, ?, ?, ?, ?,  ?, ?, ?);';
        var params = [
            userDetails.email,
            userDetails.password,
            userDetails.nameOfBusiness,
            userDetails.address,
            userDetails.phone,
            userDetails.lat,
            userDetails.long,
            0, // is_active
            accessToken,
            userDetails.primaryContactName
        ];

        var q=connection.query(sql, params, function (err) {
            if (err) {
                return callback(util.createError(err, 400, 'User save failed', q.sql));
            }
            callback(null,accessToken);

        });
    });
};

/**
 * business user login  (POST CALL) (PATH: /business/login)
 * @param {string} req.body.email email of the business
 * @param {string} req.body.password password of the business
 * @param {object} res node Response object
 */
exports.login = function (req, res, next) {
    if (!req.body.email || !req.body.password) {
        return next(util.createError(null, 422, 'Missing parameters. Unable to process.'));
    }
    var email = req.body.email;
    var password = req.body.password;
    var sql = 'select * from `business_user` where `email`=?';
    connection.query(sql, [email, password], function (err, rows) {
        if (err) {
            return next(util.createError(null, 500, 'User fetch failed'));
        }
        if (rows.length == 0) {
            return next(util.createError(null, 401, 'User not found. SignUp!'));
        }
        if (rows[0].email !== email || rows[0].password != password) {
            return next(util.createError(null, 401, 'Incorrect email or password. Check credentials'));
        }

        util.AccessToken(function (errAccessToken, accessToken) {
            if(errAccessToken){
                return next(errAccessToken);
            }
            var sqlUpdateToken= "Update `business_user` set `access_token`=? where `id`=?"
            connection.query(sqlUpdateToken,[accessToken,rows[0].id],function(errToken,rowsToken){
                if(errToken){
                    return next(util.createError(null, 500, 'Token Update Failed'));
                }
                res.send({  'Status':200,
                    'Message':'Log in successful',
                    'data':{
                        'accessToken': accessToken
                    }
                });
            });
        });
    });
};


/**
 * Second part of the profile for business user.He cannot access the admin panel without completing this step (POST CALL) (PATH: /business/profile)
 * @param {string} req.body.email email of the business
 * @param {string} req.body.token accessToken of the business
 * @param {string} req.body.username username of the business
 * @param {string} req.body.openTime openTime of the business: format: 15:00:00
 * @param {string} req.body.closeTime openTime of the business: format: 23:00:00
 * @param {string} req.body.capacity capacity of the business
 * @param {string} req.body.tabsMin tabsMin of the business
 * @param {string} req.body.beerOnTop beerOnTop of the business
 * @param {string} req.body.daysOfWeek comman seperated string of days open for business: ex "1,2" 6-sunday, 0- monday and so on
 */
exports.profile=function(req,res,next){
    var userObj = {};

    if (!req.body.username || !req.body.openTime || !req.body.closeTime
        || !req.body.capacity || !req.body.tabsMin || !req.body.beerOnTop || !req.body.daysOfWeek ) {
        return next(util.createError(null, 422, 'Missing parameters. Unable to process.'));
    }
    userObj['businessId']=req.businessId;
    userObj['username'] = req.body.username;
    userObj['profile_photo'] = (req.body.photo)?req.body.photo:'';
    userObj['open_time'] = req.body.openTime;
    userObj['close_time'] = req.body.closeTime;
    userObj['capacity'] = req.body.capacity;
    userObj['tabs_mins'] = req.body.tabsMin;
    userObj['beer_on_top'] = req.body.beerOnTop;
    userObj['daysOfWeek'] = req.body.daysOfWeek;

    thisis.secondLevelProfile(userObj, function (err) {
        if (err) {
            return next(err);
        }
        res.send({  'Status':200,
            'Message':'Business Profile Completed.',
            'data':{}
        });
    });
}

/**
 * @param {object} userDetails
 * @param {function} callback
 */
exports.secondLevelProfile = function (userDetails, callback) {

    var sql = 'Update `business_user` set `username`=? , `profile_photo`=? , `open_time`=? , `close_time`=? , `capacity`=? , ' +
        '`tabs_mins`=? , `beer_on_top`=? , `is_active`=?  where `id`=?';

    var params = [
        userDetails.username,
        userDetails.profile_photo,
        userDetails.open_time,
        userDetails.close_time,
        userDetails.capacity,
        userDetails.tabs_mins,
        userDetails.beer_on_top,
        1, // is_active
        userDetails.businessId
    ];
    var days=userDetails.daysOfWeek.split(',');

    var q=connection.query(sql, params, function (err) {
        if (err) {
            return callback(util.createError(err, 400, 'User save failed', q.sql));
        }
        if(days.length>0) {
            thisis.businessOpenDays(days, userDetails.businessId,function (errDays) {
                if (errDays) {
                    return callback(errDays);
                }
                callback(null);
            });
        }else {
            callback(null);
        }
    });

}

/**
 *
 * @param {object} days array of days open for a business : ex ["1","2"]
 * @param {string} businessId businessId for a business.
 * @param {function} callback
 */
exports.businessOpenDays = function (days,businessId,callback) {
    var sqlDelete="DELETE FROM `business_days_open` where business_id=?";

    var q1 = connection.query(sqlDelete,[businessId],function(errDel){
        if (errDel) {
            return callback(util.createError(errDel, 400, 'User save failed', q1.sql));
        }
        var insertString="";
        var params=[];
        for (var i=0;i<days.length;i++){
            insertString = insertString + " (?,?)";
            if(i<days.length-1){
                insertString = insertString + ",";
            }
            params.push(businessId);
            params.push(parseInt(days[i]));
        }

        var sql="INSERT INTO `business_days_open` (`business_id`, `days_open`) VALUES "+ insertString;

        var q = connection.query(sql,params,function(err){
            if (err) {
                return callback(util.createError(err, 400, 'User save failed', q.sql));
            }
            callback(null);
        });
    });
}