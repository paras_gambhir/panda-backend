var util = require('../commonFunctions');
var _ = require('lodash');


/**
 * Create a new offer and store in database along with an auto-generated coupon code (POST CALL) (PATH: /business/createOffer)
 * @param {string} req.body.email Email address of the user creating new offer
 * @param {string} req.body.token Access token of the user
 * @param {string} req.body.name Name of offer
 * @param {string} req.body.description Description of offer
 * @param {string} req.body.type Type of discount (0 = Flat cash discount, 1 = %age discount)
 * @param {string} req.body.cost Discount given for each offer (amount for type 0, %age for type 1)
 * @param {string} req.body.code Coupon code for redemption
 * @param {string} req.body.pointsPerRedemption Points awarded for each redemption
 * @param {string} req.body.startDate Start date for offer (YYYY-MM-DD)
 * @param {string} req.body.endDate End date for offer (YYYY-MM-DD)
 * @param {string} req.body.startTime Start time for offer (HH:mm:ss)
 * @param {string} req.body.endTime End time for offer (HH:mm:ss)
 * @param {*} req.body.daysOfWeek array / comma seperated string  of days of week for when offer is valid (0: Monday, 6: Sunday)
 * @returns {object} Success status if stored successfully otherwise error
 */

exports.storeOfferController = function(req, res, next) {
    if (!req.body['name'] || !req.body['description'] ||
        !req.body['type'] || !req.body['startDate'] ||
        !req.body['endDate'] || !req.body['startTime'] || !req.body['pointsPerRedemption'] || !req.body['endTime'] || !req.body['daysOfWeek']) {
        return next(util.createError(null, 422, 'Missing parameters. Unable to process.'));
    }
    if (typeof req.body['daysOfWeek'] === 'string') {
        req.body['daysOfWeek'] = req.body['daysOfWeek'].split(',').map(function (d) {
            return parseInt(d);
        });
    }

    var params = req.body;
    params['businessId'] = req['businessId'];
    storeOffer(params, function (err) {
        if (err) return next(err);
        res.send({ Status: 200, Message: 'stored', data:{} });
    });
}

/**
 * Create a new offer and store in database along with an auto-generated coupon code (POST CALL) (PATH: /business/editOffer)
 * @param {string} req.body.email Email address of the user creating new offer
 * @param {string} req.body.token Access token of the user
 * @param {string} req.body.name Name of offer
 * @param {string} req.body.description Description of offer
 * @param {string} req.body.type Type of discount (0 = Flat cash discount, 1 = %age discount)
 * @param {string} req.body.cost Discount given for each offer (amount for type 0, %age for type 1)
 * @param {string} req.body.code Coupon code for redemption
 * @param {string} req.body.pointsPerRedemption Points awarded for each redemption
 * @param {string} req.body.startDate Start date for offer (YYYY-MM-DD)
 * @param {string} req.body.endDate End date for offer (YYYY-MM-DD)
 * @param {string} req.body.startTime Start time for offer (HH:mm:ss)
 * @param {string} req.body.endTime End time for offer (HH:mm:ss)
 * @param {*} req.body.daysOfWeek array / comma seperated string  of days of week for when offer is valid (0: Monday, 6: Sunday)
 * @param {string} req.body.isActive 0-disabled,1-enabled
 * @returns {object} Success status if stored successfully otherwise error
 */
exports.editOfferController = function(req,res,next){
    if (!req.body['id'] || !req.body['name'] || !req.body['description'] ||
        !req.body['type'] || !req.body['startDate'] ||
        !req.body['endDate'] || !req.body['startTime'] || !req.body['pointsPerRedemption'] || !req.body['endTime'] || !req.body['daysOfWeek']) {
        return next(util.createError(null, 422, 'Missing parameters. Unable to process.'));
    }
    if (typeof req.body['daysOfWeek'] === 'string') {
        req.body['daysOfWeek'] = req.body['daysOfWeek'].split(',').map(function (d) {
            return parseInt(d);
        });
    }

    var sql= "Update `offer` set name=?,description=?, type=? ,start_date=?, end_date=?," +
        "start_time=?,end_time=?,points_per_redemption=?,is_active=? where id=?";
    var q1=connection.query(sql,[req.body['name'],req.body['description'],
        req.body['type'], req.body['startDate'], req.body['endDate'],
        req.body['startTime'], req.body['endTime'],req.body['pointsPerRedemption'],
        req.body['isActive'],req.body['id']],function(err){
        if(err)
            return next(util.createError(err, 400, 'Offer edit failed', q1.sql));
        storeOfferDays(req.body['daysOfWeek'], req['businessId'], function (errDays) {
            if (errDays) {
                var q2=connection.query('DELETE FROM offer WHERE id = (SELECT MAX(id) FROM offer WHERE business_id = ?)', [params['businessId']], function () {
                    return next(util.createError(errDays, 400, 'Offer edit failed', q1.sql));
                });
            } else {
                res.send({ Status: 200, Message: 'Update successful', data:{} });
            }
        });
    });

}

/**
 * Store offer along with days of week the offer is running. If error while storing offer days, corresponding offer is also deleted
 * @param {object} params Parameters to be stored in offer table
 * @param {function} callback Javascript callback function
 * @returns {null} Null if successful
 */
function storeOffer(params, callback) {
    var query_params = {
        'name': params['name'],
        'description': params['description'],
        'type': params['type'],
        'business_id': params['businessId'],
        'start_date': params['startDate'],
        'end_date': params['endDate'],
        'start_time': params['startTime'],
        'end_time': params['endTime'],
        'points_per_redemption': params['pointsPerRedemption'],
        'is_active': 1
    };
    var possibleChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var random_chars = _.sampleSize(possibleChars, 2);
    var sql = 'INSERT INTO offer set ?, code = CONCAT(?, LAST_INSERT_ID() + 1)';
    var q = connection.query(sql, [query_params, random_chars], function (err) {
        if (err) return callback(util.createError(err, 500, 'Offer save failed', q.sql));
        storeOfferDays(params['daysOfWeek'], params['businessId'], function (err) {
            if (err) {
                connection.query('DELETE FROM offer WHERE id = (SELECT MAX(id) FROM offer WHERE business_id = ?)', [params['businessId']], function () {
                    return callback(err);
                });
            } else {
                callback();
            }
        });
    });
}

/**
 * Store days on which offer is valid
 * @param {object} days array of days open for a business : ex ["1","2"] (0: Monday, 6: Sunday)
 * @param {string} offerId offerId for an offer.
 * @param {function} callback
 * @returns {null} Null if success
 */
function storeOfferDays(days, businessId, callback) {
    // Get offer id
    getOfferId(businessId, function (err, offerId) {
        if (err) return callback(err);
        // Delete offer days if exist
        var sqlDelete = 'DELETE FROM `offer_days` where offer_id = ?';
        var q1 = connection.query(sqlDelete, [offerId], function (errDel) {
            if (errDel) {
                return callback(util.createError(errDel, 400, 'Offer save failed', q1.sql));
            }
            var params = [];
            for (var i = 0; i < days.length; i++) {
                params.push([offerId, days[i]]);
            }
            // Insert all offer days
            var sql = 'INSERT INTO `offer_days` (`offer_id`, `day`) VALUES ?';

            var q = connection.query(sql, [params], function (err) {
                if (err) {
                    return callback(util.createError(err, 400, 'Offer save failed', q.sql));
                }
                callback();
            });
        });
    });
}

/**
 * 
 * Get the most recent offer id for a select business id 
 * @param {number} businessId Business id to search for offers
 * @param {function} callback Node callback function
 */
function getOfferId(businessId, callback) {
    var sql = 'SELECT MAX(id) AS id FROM offer WHERE business_id = ?';
    var q = connection.query(sql, [businessId], function (err, rows) {
        if (err || rows.length === 0) return callback(util.createError(err, 500, 'Offer save failed', q.sql));
        callback(null, rows[0]['id']);
    });
}

/**
 * Get offers near a user's current location (POST CALL) (PATH: /customer/getOffers)
 * @param {string} req.body.token Access token of user
 * @param {string} req.body.email Email address of user
 * @param {float} req.body.lat Current latitude of user
 * @param {float} req.body.long Current latitude of user
 */
exports.getNearbyOffersController=function(req, res, next) {
    var params = req.body;
    params['lat'] = parseFloat(params['lat']);
    params['long'] = parseFloat(params['long']);
    getNearbyOffers(params, function(err, offers) {
        if (err) return next(err);
        res.send(offers);
    });
}

function getNearbyOffers(params, callback) {
    var query_params = [params['lat'], params['lat'], params['long']];
    var sql = 'SELECT * FROM offer WHERE business_id IN (SELECT id FROM business_user u ' +
        'WHERE ( ' +
        '          acos(sin(u.lat * 0.0175) * sin(? * 0.0175)  ' +
        '               + cos(u.lat * 0.0175) * cos(? * 0.0175) *     ' +
        '                 cos((? * 0.0175) - (u.long * 0.0175)) ' +
        '              ) * 3959 <= 5 ' +
        '      )) AND TIME(NOW()) BETWEEN start_time AND end_time AND id IN (SELECT offer_id FROM `offer_days` WHERE DAYOFWEEK(NOW()) = `day`)';
    var q = connection.query(sql, query_params, function (err, rows) {
        if (err) return callback(util.createError(err, 500, 'Error while fetching offers', q.sql));
        if (rows.length === 0) return callback(util.createError(null, 500, 'No offers nearby', q.sql));
        callback(null, rows);
    });
}

/**
 * /**
 * Get offers for a business (POST CALL) (PATH: /business/getOffers)
 * @param {string} req.query.email Email address of the user creating new offer
 * @param {string} req.query.token Access token of the user
 * @param {string} req.query.startDate Start date for offer (YYYY-MM-DD) - optional
 * @param {string} req.query.endDate Start date for offer (YYYY-MM-DD) - optional
 * @param {string} req.query.startTime Start date for offer (HH:MM:SS) - optional
 * @param {string} req.query.endTime Start date for offer (HH:MM:SS) - optional
 */
exports.getOffersforBusiness=function(req,res,next){
    var startDate=(req.body.startDate?"'"+req.body.startDate+"'":'now()');
    var endDate=(req.body.endDate?"'"+req.body.endDate+"'":'now()');
    var startTime=(req.body.startTime?"'"+req.body.startTime+"'":'current_time');
    var endTime=(req.body.startTime?"'"+req.body.endTime+"'":'current_time');
 var sql="select *,timediff(`end_time`,CURRENT_TIME) AS time_remaining from `offer` where business_id=5 and `is_active`=1 " +
     "and "+startDate+">=`start_date` and "+endDate+"<=`end_date` and "+startTime+">=`start_time` and "+endTime+"<=`end_time`;";
    var q= connection.query(sql,[],function(err,data){
        console.log(q.sql);
        if(err){
            return next(err);
        }
        res.send({
            "Status":200,
            "Message":"List of Offers",
            "data":data
        });
    });

}