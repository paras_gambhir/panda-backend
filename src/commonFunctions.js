/**
 * Created by harmandeepkaur on 29/10/16.
 */

var crypto= require('crypto');

exports.AccessToken=function(callback){
    crypto.randomBytes(20, function(err, buffer) {
        if(err){
            callback(createError(err, 500));
        }else {
            var token = buffer.toString('hex');
            callback(null,token);
        }
    });
};

exports.getBusinessIDfromAcessToken=function(req,res,next){
    var email,token;
    if(req.body){
        email=req.body.email;
        token=req.body.token;
    }else{
        email=req.query.email;
        token=req.query.token;
    }
    var sql='select * from `business_user` where `email`=? and `access_token`=?';
    var q = connection.query(sql,[email,token],function(err,rows){
        if(err|| rows.length==0){
            return next(createError(err, 401, 'Unauthorized Request', q.sql));
        }else {
            req.businessId=rows[0]['id'];
            next();
        }
    });
};

exports.getCustomerIDfromAcessToken=function(req,res,next){
    var email,token;
    if(req.body){
        email=req.body.email;
        token=req.body.token;
    }else{
        email=req.query.email;
        token=req.query.token;
    }
    var sql='select * from `customer` where `email`=? and `access_token`=?';
    var q = connection.query(sql,[email,token],function(err,rows){
        if(err|| rows.length==0){
            return next(createError(err, 401, 'Unauthorized Request', q.sql));
        }else {
            req.customerId=rows[0]['id'];
            next();
        }
    });
};

function createError(err, status, context, query) {
    if (!err) {
        err = {};
    }
    if (!err['status'] && status) {
        err['status'] = status;
    }
    if (!err['context'] && context) {
        err['context'] = context;
    }
    if (!err['query'] && query) {
        err['query'] = query;
    }
    return err;
}

exports.createError = createError;