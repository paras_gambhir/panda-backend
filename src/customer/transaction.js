/**
 * Created by harmandeepkaur on 31/10/16.
 */

var util = require('../commonFunctions');
var thisis = this;

/**
 * (POST CALL) (PATH: /customer/redeemCoupon)
 * @param {string} req.body.code promo code of the redeemed offer
 * @param {string} req.body.didnotWork (optional) if user chooses not working: format '1'
 * @param {string} req.body.upVote (optional) if user chooses not working: format '1'
 */
exports.redeemCoupon = function (req, res, next){
    var transactionDetails={};
    if(!req.body.code){
        return next(util.createError(null, 422, 'Missing parameters. Unable to process.'));
    }

    transactionDetails["offerId"] = parseInt(req.body.code.substring(2));
    transactionDetails["customerId"] = req.customerId;
    transactionDetails["didnotWork"] = (req.body.didnotWork)?1:0;
    transactionDetails["upVote"] = (req.body.upVote)?1:0;

    thisis.redeem(transactionDetails,function(err){
        if(err){
            return next(util.createError(null, err.status, err.context));
        }
        res.send("Coupon redemeed successfully.");
    });
}

exports.redeem = function(transaction,callback){
    //TODO:
    //check if the offer has not expired
    // fetch offer points
    //increment the counter of redemptions in offer
    //increment redemptions in user and add points to user
    var sql="select * from `offer` where `id`=? and `is_active`=? " +
        "and now()>=`start_date` and now()<=`end_date` " +
        "and current_time>=`start_time` and current_time<=`end_time`";
    var q= connection.query(sql, [transaction.offerId,1], function(err,rows){
        console.log(q.sql);
        if (err) {
            return callback(util.createError(null, 500, 'Offer fetch failed'));
        }
        if (rows.length == 0) {
            return callback(util.createError(null, 401, 'Offer not valid!'));
        }
        var offerPoints = parseInt(rows[0]["points_per_redemption"]);
        var offerRedeemptions = parseInt(rows[0]["redemptions"]);

        var sqlDaysCheck="select * from `offer_days` where `offer_id`=? and `day`=WEEKDAY(now())";
        connection.query(sqlDaysCheck, [transaction.offerId], function(errDays,rowsDays) {
            if (errDays) {
                return callback(util.createError(null, 500, 'Offer fetch failed'));
            }
            if (rowsDays.length == 0) {
                return callback(util.createError(null, 401, 'Offer not valid!'));
            }

            var sqlOffersUpdate ="Update `offer` set `redemptions`=? where `id`=?";
            connection.query(sqlOffersUpdate,[offerRedeemptions+1,transaction.offerId],function(errOffer){
                if (errOffer) {
                    return callback(util.createError(null, 500, 'Unable to set offer redemptions'));
                }
                var sqlCustomerUpdate="Update `customer` set `redemptions`=`redemptions`+1 , `points`= `points`+ ? where `id`=?";
                var q1= connection.query(sqlCustomerUpdate,[offerPoints,transaction.customerId], function (errCustomer){
                    console.log(q1.sql);
                    if (errCustomer) {
                        return callback(util.createError(null, 500, 'Unable to set customer redemptions'));
                    }
                    var sqlTransaction="INSERT INTO `transaction` " +
                        "(`offer_id`, `customer_id`, `didnot_work`, `upvote`) " +
                        "VALUES (?, ?, ?, ?)";
                    connection.query(sqlTransaction,
                        [transaction.offerId,transaction.customerId,transaction.didnotWork,transaction.upVote],
                        function(errTransactions){
                            if (errTransactions) {
                                return callback(util.createError(null, 500, 'Unable to save trnsactions data'));
                            }
                            return callback(null);
                    });
                });
            });
        });
    });
}