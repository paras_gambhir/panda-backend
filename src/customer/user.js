/**
 * Created by harmandeepkaur on 31/10/16.
 */

var util = require('../commonFunctions');
var thisis = this;

/**
 * business user signup page call (POST CALL) (PATH: /customer/signup)
 * @param {string} req.body.firstName firstName of the user
 * @param {string} req.body.lastName lastName of the user
 * @param {string} req.body.email email of the user
 * @param {string} req.body.password Encrypted password of the user
 * @param {float} req.body.lat latitude of the user
 * @param {float} req.body.long longitude of the user
 */
exports.signup = function (req, res, next) {
    var userObj = {};

    if (!req.body.firstName || !req.body.lastName || !req.body.email || !req.body.password
        || !req.body.lat || !req.body.long ) {

        return next(util.createError(null, 422, 'Missing parameters. Unable to process.'));
    }

    userObj['firstName'] = req.body.firstName;
    userObj['lastName'] = req.body.lastName;
    userObj['email'] = req.body.email;
    userObj['password'] = req.body.password;
    userObj['lat'] = req.body.lat;
    userObj['long'] = req.body.long;

    thisis.signupCustomer(userObj, function (err,accessToken) {
        if (err) {
            return next(err);
        }
        res.send({ 'accessToken': accessToken });
    });
};

/**
 *
 * @param {object} userDetails
 * @param {function} callback
 */
exports.signupCustomer = function (userDetails, callback) {

    util.AccessToken(function (errAccessToken, accessToken) {

        if(errAccessToken){
            return callback(errAccessToken);
        }

        var sql = 'INSERT INTO `customer` (`first_name`, `last_name`, `email`, `password`, `access_token`, ' +
            ' `is_active`, `latitude`, `longitude`) ' +
            "VALUES (?,?,?,?,?,?,?,?)";
        var params = [
            userDetails.firstName,
            userDetails.lastName,
            userDetails.email,
            userDetails.password,
            accessToken,
            1, //is_active
            userDetails.lat,
            userDetails.long
        ];

        var q=connection.query(sql, params, function (err) {
            if (err) {
                return callback(util.createError(err, 400, 'customer save failed', q.sql));
            }
            callback(null,accessToken);
        });
    });
};

/**
 * Customer login (POST CALL) (PATH: /customer/login)
 * @param {string} req.body.email email of the business
 * @param {string} req.body.password password of the business
 */
exports.login = function (req, res, next) {
    if (!req.body.email || !req.body.password) {
        return next(util.createError(null, 422, 'Missing parameters. Unable to process.'));
    }
    var email = req.body.email;
    var password = req.body.password;
    var sql = 'select * from `customer` where `email`=?';
    connection.query(sql, [email, password], function (err, rows) {
        if (err) {
            return next(util.createError(null, 500, 'User fetch failed'));
        }
        if (rows.length == 0) {
            return next(util.createError(null, 401, 'User not found. SignUp!'));
        }
        if (rows[0].email !== email || rows[0].password != password) {
            return next(util.createError(null, 401, 'Incorrect email or password. Check credentials'));
        }
        util.AccessToken(function (errAccessToken, accessToken) {
            if(errAccessToken){
                return next(errAccessToken);
            }
            var sqlUpdateToken= "Update `customer` set `access_token`=? where `id`=?"
            connection.query(sqlUpdateToken,[accessToken,rows[0].id],function(errToken,rowsToken){
                if(errToken){
                    return next(util.createError(null, 500, 'Token Update Failed'));
                }
                res.send({ 'accessToken': accessToken });
            });
        });
    });
};