var config = require('config');
var mysql = require('mysql');
var db_config_panda = {
    host: config.get('dBSettings.pandaDb').host,
    user: config.get('dBSettings.pandaDb').user,
    password: config.get('dBSettings.pandaDb').password,
    database: config.get('dBSettings.pandaDb').dbName
};

function handleDisconnect_panda() {
    connection = mysql.createConnection(db_config_panda); // Recreate the connection, since
    // the old one cannot be reused.

    connection.connect(function(err) { // The server is either down
        if (err) { // or restarting (takes a while sometimes).
            console.log('error when connecting to db (panda):', err);
            setTimeout(handleDisconnect_panda, 2000); // We introduce a delay before attempting to reconnect,
        } // to avoid a hot loop, and to allow our node script to
    }); // process asynchronous requests in the meantime.
    // If you're also serving http, display a 503 error.
    connection.on('error', function(err) {
        console.log('db error', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST' || err.code === 'PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR') { // Connection to the MySQL server is usually
            handleDisconnect_panda(); // lost due to either server restart, or a
        } else { // connnection idle timeout (the wait_timeout
            if (err.fatal) {
                console.log('\n\n Fatal error occurred:\n ', err);
                handleDisconnect_panda(); // lost due to either server restart, or a
            } else {
                throw err; // server variable configures this)

            }
        }
    });
}
handleDisconnect_panda();